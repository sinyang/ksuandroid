package com.infinity.KSUAndroid;


import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;

import java.util.Calendar;

public class MainActivity extends ActionBarActivity implements CommonFunctions {


    /*this is in the proper order for sodexo..
    * though monday is the first in the array, its INT value is 2.
    * the values are from sunday to saturday, 1 to 7... so in this case, array[0] = 2*/
    int[] gregorian_day_list = {Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY, Calendar.SATURDAY, Calendar.SUNDAY};
    LeftDrawerFragment leftDrawerFragment;
    DrawerLayout mDrawerLayout;
    TextView actionBarTitle;
    int drawerStatus;
    private ProgressBar spinner;
    FragmentManager manager;
    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("JIM", "Main begin");
        setContentView(R.layout.activity_main_cover_appbar);

        Log.d("JIM", "Main begin");
        //query system for meal and time
        setDayandMealtoCurrent();

        Fragment frag;
        /*
            set pkey to 0 so app never starts on nutrition fragment..avoids issues.
            start the fragment early(it does network)]
         */
        saveBackendPrefs("pkey", 0);
        frag = new GenericListViewFragment();

        //set UI
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setScrimColor(Color.TRANSPARENT);


        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar); //set toolbar as actionbar
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        actionBarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title_text);
        actionBarTitle.setText(R.string.app_name);

        manager = getSupportFragmentManager();

        //instantiate the drawer fragment linking to fragment in activity_main.xml
        leftDrawerFragment = (LeftDrawerFragment) manager.findFragmentById(R.id.fragment_left_drawer);

        //method we created which gets passed the drawer layout and the toolbar
        leftDrawerFragment.setUp(R.id.fragment_left_drawer, mDrawerLayout, toolbar);

        final DayMealChooserFragment dayMealChooserFragment = (DayMealChooserFragment) manager.findFragmentById(R.id.fragment_day_meal_chooser);
        dayMealChooserFragment.setUp(R.id.fragment_day_meal_chooser, mDrawerLayout);

        /*==================================================================
        * Setting toolbar
        * ------------------------------------------------------------------
        * */


        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {
                View darkOverlay;
                View centerView = findViewById(R.id.center_navigation_view);
                float width = getResources().getDimension(R.dimen.nav_drawer_width);
                float offset = 0;

                if (v > 0) {
                    offset = width * (float) Math.pow(v, 1.8);
                }
//                if (v > .25){
//                    offset = width * (v - (float).25) * (float)(1/0.75);
//                }

                if (view.getId() == R.id.fragment_left_drawer) {
                    darkOverlay = view.findViewById(R.id.left_cover_overlay);
                    mDrawerLayout.bringChildToFront(centerView);
                    centerView.setTranslationX(offset);
                } else {
                    darkOverlay = view.findViewById(R.id.right_cover_overlay);
                    centerView.setTranslationX(-offset);
                    mDrawerLayout.bringChildToFront(centerView);
                }
                darkOverlay.setAlpha(1 - v);
            }

            @Override
            public void onDrawerOpened(View view) {
                float width = getResources().getDimension(R.dimen.nav_drawer_width);
                View centerView = findViewById(R.id.center_navigation_view);
                if (view.getId() == R.id.fragment_left_drawer) {
                    mDrawerLayout.bringChildToFront(view);
                    drawerStatus = 1;

                } else {
                    mDrawerLayout.bringChildToFront(view);
                    drawerStatus = 2;
                }

            }

            @Override
            public void onDrawerClosed(View view) {
                if (drawerStatus == 2) {
                    dayMealChooserFragment.refreshData();
                }
                drawerStatus = 0;
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });

        final ImageView rightDrawerSwitch = (ImageView) toolbar.findViewById(R.id.toolbar_right_drawer_button);
        final View leftDrawerSwitch = toolbar.findViewById(R.id.toolbar_left_drawer_button);

//        spinner = (ProgressBar)mDrawerLayout.findViewById(R.id.progressBar1);

        //setting toolbar button touch events...
        rightDrawerSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerStatus == 1) {
                    leftDrawerFragment.closeDrawer();
                    dayMealChooserFragment.openDrawer();
                } else {
                    if (drawerStatus == 2) {
                        dayMealChooserFragment.closeDrawer();
                    } else {
                        dayMealChooserFragment.openDrawer();
                    }
                }

            }
        });

        rightDrawerSwitch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                rightDrawerSwitch.setBackgroundResource(R.color.secondaryColorTrans);

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        rightDrawerSwitch.setBackgroundResource(R.color.secondaryColorTrans);
                        return false;

                    case MotionEvent.ACTION_UP:
                        // back to normal state
                        rightDrawerSwitch.setBackgroundResource(R.color.transparent);
                        return false;

                    case MotionEvent.ACTION_MOVE:
//                        rightDrawerSwitch.setBackgroundResource(R.color.tranparant);
                        return false;
                    default:
                        return true;
                }
            }
        });

        leftDrawerSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerStatus == 2) {
                    dayMealChooserFragment.closeDrawer();
                    leftDrawerFragment.openDrawer();
                } else {
                    if (drawerStatus == 1) {
                        leftDrawerFragment.closeDrawer();
                    } else {
                        leftDrawerFragment.openDrawer();
                    }
                }

            }
        });

        leftDrawerSwitch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                leftDrawerSwitch.setBackgroundResource(R.color.secondaryColorTrans);

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        leftDrawerSwitch.setBackgroundResource(R.color.secondaryColorTrans);
                        return false;

                    case MotionEvent.ACTION_UP:
                        // back to normal state
                        leftDrawerSwitch.setBackgroundResource(R.color.transparent);
                        return false;

                    case MotionEvent.ACTION_MOVE:
//                        rightDrawerSwitch.setBackgroundResource(R.color.tranparant);
                        return false;
                    default:
                        return true;
                }
            }
        });

        /*------------------------------------------------------------------
        * END Setting toolbar
        * =================================================================*/


        Log.d("JIM", "Main 2");
        //make make the fragment visible
        changeFragment(frag);
    }


    @Override
    public void startProgressSpinner() {
        spinner.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void stopProgressSpinner() {
        spinner.setVisibility(ProgressBar.GONE);
    }

    public void changeFragment(Fragment frag) {

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
//        transaction.setCustomAnimations(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top);
        transaction.replace(R.id.content, frag);
        transaction.commit();

    }

    public void pushFragment(Fragment frag) {

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
//        transaction.setCustomAnimations(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top);
        transaction.add(R.id.content, frag);
        transaction.addToBackStack(null);
        transaction.commit();

    }


    @Override
    public void onBackPressed() {
        System.out.println(manager.getBackStackEntryCount());

        if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack();

        } else {
            Log.d("JIM", "BackPress begin");
            SharedPreferences preferences = getSharedPreferences("backendPrefs", Context.MODE_PRIVATE);
            if (preferences.getInt("pkey", 0) > 0) {
                Log.d("JIM", "BackPress pkeyset");
                saveBackendPrefs("pkey", 0);
            } else if (!preferences.getString("venue", "").equals("")) {
                Log.d("JIM", "BackPress venue set");
                saveBackendPrefs("venue", "");
                saveBackendPrefs("venueFullName", "");
            } else {
                Log.d("JIM", "BackPress bout to call finish");
                finish();
            }
            Log.d("JIM", "BackPress new frag begin");
            Fragment frag = new GenericListViewFragment();
            manager.beginTransaction().replace(R.id.content, frag, null).commit();
        }

        Log.d("JIM", "BackPress FINISH");
    }


    @Override
    protected void onResume() {
        super.onResume();
        setDayandMealtoCurrent();
    }

    private void setDayandMealtoCurrent() {
        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        Log.d("JIM", "Gregorian Day = " + day);

        TextView mealView = (TextView) findViewById(R.id.meal_type_textview);
        TextView dayView = (TextView) findViewById(R.id.day_of_week_textview);
        String[] days_of_the_week_strings = getResources().getStringArray(R.array.day_array);
        String[] meals_strings = getResources().getStringArray(R.array.meal_array);

        switch (day) {
            case 1:
                dayView.setText(days_of_the_week_strings[6]);
                saveBackendPrefs("currentDayString", days_of_the_week_strings[6]);
                break;
            case 2:
                dayView.setText(days_of_the_week_strings[0]);
                saveBackendPrefs("currentDayString", days_of_the_week_strings[0]);
                break;
            case 3:
                dayView.setText(days_of_the_week_strings[1]);
                saveBackendPrefs("currentDayString", days_of_the_week_strings[1]);
                break;
            case 4:
                dayView.setText(days_of_the_week_strings[2]);
                saveBackendPrefs("currentDayString", days_of_the_week_strings[2]);
                break;
            case 5:
                dayView.setText(days_of_the_week_strings[3]);
                saveBackendPrefs("currentDayString", days_of_the_week_strings[3]);
                break;
            case 6:
                dayView.setText(days_of_the_week_strings[4]);
                saveBackendPrefs("currentDayString", days_of_the_week_strings[4]);
                break;
            case 7:
                dayView.setText(days_of_the_week_strings[5]);
                saveBackendPrefs("currentDayString", days_of_the_week_strings[5]);
                break;
        }

        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int minutes = Calendar.getInstance().get(Calendar.MINUTE);
        if (hour < 10) {
            saveBackendPrefs("meal", 1);
            mealView.setText(meals_strings[0]);
        } else if (hour == 10 && minutes < 30) {
            saveBackendPrefs("meal", 1);
            mealView.setText(meals_strings[0]);
        } else if (hour > 16) {
            saveBackendPrefs("meal", 3);
            mealView.setText(meals_strings[2]);
        } else if (hour == 16 && minutes >= 30) {
            saveBackendPrefs("meal", 3);
            mealView.setText(meals_strings[2]);
        } else {
            saveBackendPrefs("meal", 2);
            mealView.setText(meals_strings[1]);
        }

        for (int i = 0; i < gregorian_day_list.length; i++) {
            if (day == gregorian_day_list[i]) {
                //sodexo starts counting at 1, which is monday..sunday is 7
                saveBackendPrefs("day", i + 1);
                /*first is for getting data from network... second is for validating
                  day selected from right drawer with current day. For that reason, we do not
                  need to add 1 to i.
                 */
                saveBackendPrefs("currentDayInt", i);
                //quit when we find one
                return;
            }
        }

        //you should never get here
        Log.d("JIM", "setDayandMealtoCurrent FINISH");
        Log.d("JIM", "setDayandMealtoCurrent How did i get here");
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    private void saveBackendPrefs(String key, int value) {
        SharedPreferences preferences = getSharedPreferences("backendPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    private void saveBackendPrefs(String key, String value) {
        SharedPreferences preferences = getSharedPreferences("backendPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

}
