package com.infinity.KSUAndroid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.infinity.KSUAndroid.GeneralClasses.CommonsAPI;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class NutritionFragment extends Fragment {
    Future<ArrayList<?>> future;
    TextView[] nutritionInfo;
    ImageView addCalorieButton;
    //ArrayList<String> returnedArrayList;

    //preference file names
    static final String BACKEND_PREF_FILE_NAME = "backendPrefs";
    static final String HEALTH_PREF_FILE_NAME = "healthPrefs";

    //preference key
    static final String PREF_CAL_COUNTER = "prefCalCounter";
    static final String PREF_KEY_LIST = "keyList";
    final static String PREF_LAST_VISITED_DATE = "lastVisitedDate";

    /*
        current day text and current day int are the values representing
        what the actual day is. Variable day on the other hand represents
        the value of the day that hte user chose to view information on.
     */
    private String weekOf;
    private int currentDayInt;
    private int day;
    private int meal;
    private String keyString;
    private String healthPrefKeyList;
    private String valueString;
    private String venueString;

	/* MAIN ACTIVITY
    * ---------------------------------------------------------
	* in main activity... make preference file name called healthPrefs...
	* keys will be the first day of the weeks..being monday according to Sodexo...
	* We will check if there is data for the file, if not, we make a string and
	* append the date of the first day of the current week.
	*
	* NUTRITION FRAGMENT
	* -----------------------------------------------------
	* String builder...The string value will be a "list" seperated by the '|' character.
	* Items will be appended to the string as follows.
	* Day
	* Meal Type
	* Item Name
	* Calories
	* Vitamin A
	* Vitamin C
	* Iron
	* Calcium
	*
	* MEAL TRACKER FRAGMENT
	* --------------------------------------------------
	* On the meal tracker fragment,
	* */


    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle instance) {
        //call CommonsAPI ASAP, give the network thread as much time as possible
        ExecutorService service = Executors.newSingleThreadExecutor();
        CommonsAPI c = new CommonsAPI(getActivity());
        future = service.submit(c);


        View n = inflater.inflate(R.layout.nutrition_layout, container, false);
        addCalorieButton = (ImageView) n.findViewById(R.id.add_calorie_button);


        //..... i copied this from iury's old code....
        nutritionInfo =
                new TextView[]{
                        (TextView) n.findViewById(R.id.ItemName),
                        (TextView) n.findViewById(R.id.ItemType),
                        (TextView) n.findViewById(R.id.CaloriesET),
                        (TextView) n.findViewById(R.id.fatCaloriesET),
                        (TextView) n.findViewById(R.id.TotalFatGrams),
                        (TextView) n.findViewById(R.id.TotalFatPercent),
                        (TextView) n.findViewById(R.id.SaturatedFatGrams),
                        (TextView) n.findViewById(R.id.StaturatedFatPercent),
                        (TextView) n.findViewById(R.id.TransFatGrams),
                        (TextView) n.findViewById(R.id.CholesterolGrams),
                        (TextView) n.findViewById(R.id.CholesterolPercent),
                        (TextView) n.findViewById(R.id.SodiumGramgs),
                        (TextView) n.findViewById(R.id.SodiumPercent),
                        (TextView) n.findViewById(R.id.TotalCarbohydratesGrams),
                        (TextView) n.findViewById(R.id.TotalCarbohydratesPercent),
                        (TextView) n.findViewById(R.id.DietaryFiberGrams),
                        (TextView) n.findViewById(R.id.DietaryFiberPercent),
                        (TextView) n.findViewById(R.id.SugarGrams),
                        (TextView) n.findViewById(R.id.ProteinGrams),
                        (TextView) n.findViewById(R.id.VitaminAPercent),
                        (TextView) n.findViewById(R.id.VitaminCPercent),
                        (TextView) n.findViewById(R.id.CalciumPercent),
                        (TextView) n.findViewById(R.id.IronPercent),
                        (TextView) n.findViewById(R.id.allergensTV),
                        (TextView) n.findViewById(R.id.IngredientsTV)
                };

        //yeah I cast here, because I trust the server API I wrote.
        ArrayList<String> nutritionGets = null;
        try {
            nutritionGets = (ArrayList<String>) future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        //25
        Log.d("JIM", "Nutrition fragement loop nutritionInfo = " + nutritionInfo.length + " nutritionGets = " + (nutritionGets != null ? nutritionGets.size() : 0));
        //hardcoding a 25 looks like a great way to null pointer!
        for (int i = 0; i < 25; i++) {
            nutritionInfo[i].setText(nutritionGets != null ? nutritionGets.get(i) : null);
        }


        //these can be instantiated last...

        //get backend pref
        getBackendPrefs();

        /*make sure the day/meal chooser drawer button is visible and change title to current venue name*/
        Toolbar toolbar = ((MainActivity) getActivity()).getToolbar();
        TextView actionBarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title_text);
        View dayMealButton = toolbar.findViewById(R.id.toolbar_right_drawer_button);
        actionBarTitle.setText(venueString);
        dayMealButton.setVisibility(View.VISIBLE);

        //determine if addCalorie button should show based on selected day validation
        if (day != currentDayInt) {
            addCalorieButton.setVisibility(View.INVISIBLE);
        }

        return n;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        weekOf = "" + getMonthString(Calendar.getInstance().get(Calendar.MONTH) + 1) +
                (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) - day) +
                Calendar.getInstance().get(Calendar.YEAR);

		/*format is mmddyyyydaymeal , where day is the int value of the current day of
		* week, and meal is the int value of the current selected meal type*/
        keyString = weekOf + day + meal;
        getHealthPreference();

        addCalorieButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//				builder.setTitle("Hello");
                builder.setMessage("Add item calories to Meal Tracker?");
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (!healthPrefKeyList.contains(keyString)) {
                            healthPrefKeyList += keyString + "::";
                            setHealthPreference(PREF_KEY_LIST, healthPrefKeyList);
                            setHealthPreference(PREF_LAST_VISITED_DATE, weekOf + day);
                        }


						/* valueString = ItemName|Calories|TotalFat|Carbohydrates|Protein|*/
                        valueString += nutritionInfo[0].getText() + "::" + nutritionInfo[2].getText()
                                + "::" + nutritionInfo[4].getText() + "::" + nutritionInfo[13].getText()
                                + "::" + nutritionInfo[18].getText() + "::";
                        Log.d("Sam", "So i made it to the VALUEtext: " + valueString);


                        setHealthPreference(keyString, valueString);
                        Toast.makeText(getActivity().getApplicationContext(), nutritionInfo[0].getText() + "added to" + getMealType() + " list.", Toast.LENGTH_LONG).show();
                    }
                });
//				builder.create();
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    /*method used to make month formated to MM..*/
    private String getMonthString(int value) {
        //make string resource in android instead of an array here..
        String returnString = "";
        if (value < 10) {
            returnString += "0";
        }
        returnString += value;

        return returnString;
    }

    private String getMealType() {
        switch (meal) {
            case 1:
                return "breakfast";
            case 2:
                return "lunch";
            case 3:
                return "dinner";
            default:
                return "lunch";
        }
    }

    private void setHealthPreference(String key, String value) {
        SharedPreferences preferences = getActivity().getSharedPreferences(HEALTH_PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private void getHealthPreference() {
        SharedPreferences preferences = getActivity().getSharedPreferences(HEALTH_PREF_FILE_NAME, Context.MODE_PRIVATE);
        healthPrefKeyList = preferences.getString(PREF_KEY_LIST, "");
        valueString = preferences.getString(keyString, "");
    }

    private void getBackendPrefs() {
        SharedPreferences preferences = getActivity().getSharedPreferences(BACKEND_PREF_FILE_NAME, Context.MODE_PRIVATE);
        currentDayInt = preferences.getInt("currentDayInt", 0);
        day = preferences.getInt("day", 0) - 1;
        meal = preferences.getInt("meal", 1);
        venueString = preferences.getString("venueFullName", "");
    }
}