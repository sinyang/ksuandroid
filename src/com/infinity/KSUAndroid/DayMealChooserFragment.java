package com.infinity.KSUAndroid;

import android.support.v4.app.FragmentActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.infinity.KSUAndroid.ArrayAdapters.DrawerArrayAdapter;

/**
 * Created by Sam.I on 12/20/2014.
 */
public class DayMealChooserFragment extends Fragment{
    static FragmentActivity mainActivity;
    ListView mealList;
    ListView dayList;
    private DrawerLayout mDrawerLayout;
    private View containerView;

    boolean hasChanged = false;

    private int previousSelectedMeal = 0;
    private int previousSelectedDay = 0;
    private int currentSelectedMeal = 0;
    private int currentSelectedDay = 0;
    private int pkey;

    private static final String PREF_FILE_NAME = "backendPrefs";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainActivity = getActivity();
        String[] drawerDayStrings = getResources().getStringArray(R.array.day_array);
        String[] drawerMealStrings = getResources().getStringArray(R.array.meal_array);


        View view = inflater.inflate(R.layout.fragment_day_meal_type, container, false);
        mealList = (ListView) view.findViewById(R.id.right_drawer_list_meal_type);
        dayList = (ListView) view.findViewById(R.id.right_drawer_list_day);

        mealList.setAdapter(new DrawerArrayAdapter(mainActivity, drawerMealStrings, 'm'));
        dayList.setAdapter(new DrawerArrayAdapter(mainActivity, drawerDayStrings, 'd'));


        mealList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View itemClicked, int position, long l) {
                int startIndex = adapterView.getFirstVisiblePosition();
                //subtract both the start and end index by the offset
                int endIndex = adapterView.getLastVisiblePosition() - startIndex;
                for(;startIndex <= endIndex; startIndex++ ){
                    if (adapterView.getChildAt(startIndex) != null) {
                        adapterView.getChildAt(startIndex).setBackgroundResource(R.color.transparent);
                    }
                }
                itemClicked.setBackgroundResource(R.color.secondaryColorTrans);
                saveBackendPrefs("meal", position + 1);
                hasChanged = true;
            }
        });

        dayList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View itemClicked, int position, long id) {

                /*
                getFirstVisiblePosition() will give you the index of the first item, but the index is
                relative to the listview indexes, rather than the adapterview. The adapterView.getChildAt()
                uses indexes relative to the adapterview, so an offset needs to be calculated to change the
                listview relative indexes to adapterview relative indexes.
                In this case, the offset will be the startIndex.
                 */
                int startIndex = adapterView.getFirstVisiblePosition();
                //subtract both the start and end index by the offset
                int endIndex = adapterView.getLastVisiblePosition() - startIndex;
                startIndex -= startIndex;
                for(;startIndex <= endIndex; startIndex++ ){
                    if (adapterView.getChildAt(startIndex) != null) {
                        adapterView.getChildAt(startIndex).setBackgroundResource(R.color.transparent);
                    }
                }
                itemClicked.setBackgroundResource(R.color.secondaryColorTrans);
                saveBackendPrefs("day", position + 1);
                hasChanged = true;
            }
        });

        return view;
    }

    public void setUp(int fragmentID, DrawerLayout drawerLayout) {

        containerView = mainActivity.findViewById(fragmentID);
        mDrawerLayout = drawerLayout; //sets as the layout that was passed
    }

    void refreshData() {
        if (hasChanged){
            if (pkey > 0) {
                saveBackendPrefs("pkey", 0);
                saveBackendPrefs("venue", "");
                saveBackendPrefs("venueFullName", "");
            }
            hasChanged = false;
            GenericListViewFragment frag = new GenericListViewFragment();
            mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.content, frag, null).commit();
        }
    }


    void closeDrawer() {
        mDrawerLayout.closeDrawer(containerView);
    }

    void openDrawer() {
        mDrawerLayout.openDrawer(containerView);
        loadBackEndPref();
    }

    private static void saveBackendPrefs(String key, int value)
    {
        SharedPreferences preferences = mainActivity.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }
    private static void saveBackendPrefs(String key, String value)
    {
        SharedPreferences preferences = mainActivity.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void loadBackEndPref () {
        SharedPreferences sharedPreferences = mainActivity.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        previousSelectedDay = (sharedPreferences.getInt("day", 0) - 1);
        previousSelectedMeal = (sharedPreferences.getInt("meal", 0) - 1);
        currentSelectedDay = (sharedPreferences.getInt("day", 0) - 1);
        currentSelectedMeal = (sharedPreferences.getInt("meal", 0) - 1);
        pkey = sharedPreferences.getInt("pkey", 0);
    }


}