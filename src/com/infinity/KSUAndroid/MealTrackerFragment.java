package com.infinity.KSUAndroid;

import android.app.Activity;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Created by Sam.I on 1/20/2015.
 */
public class MealTrackerFragment extends Fragment {
    View view;
    TableLayout tableLayout;

    //preference file names
    static final String BACKEND_PREF_FILE_NAME = "backendPrefs";
    static final String HEALTH_PREF_FILE_NAME = "healthPrefs";
    //preference key
    static final String PREF_KEY_LIST = "keyList";
    final static String PREF_LAST_VISITED_DATE = "lastVisitedDate";

    private String healthPrefKeyList;
//    private String breakfast;
//    private String lunch;
//    private String dinner;
    private FragmentActivity mainActivity;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_meal_tracker, container, false);

        /*hide the day/meal chooser drawer button*/
        Toolbar toolbar = ((MainActivity) getActivity()).getToolbar();
        TextView actionBarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title_text);
        View dayMealButton = toolbar.findViewById(R.id.toolbar_right_drawer_button);
        actionBarTitle.setText(R.string.meal_tracker);
        dayMealButton.setVisibility(View.GONE);


        tableLayout = (TableLayout) view.findViewById(R.id.meal_tracker_table_inner);


        String lastVisitedDate = getHealthPreference(PREF_LAST_VISITED_DATE, "");
        healthPrefKeyList = getHealthPreference(PREF_KEY_LIST, "");

        String breakfast = getHealthPreference(lastVisitedDate +1, "");
        String lunch = getHealthPreference(lastVisitedDate +2, "");
        String dinner = getHealthPreference(lastVisitedDate +3, "");

        setTableData(breakfast, 1);
        setTableData(lunch, 2);
        setTableData(dinner, 3);


//        breakfast = getHealthPreference(lastVisitedDate+1, "");
//        lunch = getHealthPreference(lastVisitedDate+2, "");
//        dinner = getHealthPreference(lastVisitedDate+3, "");





//        String[] keyArray = healthPrefKeyList.split("|");
//
//        for (String s: keyArray) {
//
//        }

        return view;
    }

    private void setTableData(String mealType, int mealNumber) {
        TextView mealTV;

        switch (mealNumber) {
            case 1:
                mealTV = (TextView) view.findViewById(R.id.b1);
                break;
            case 2:
                mealTV = (TextView) view.findViewById(R.id.l1);
                break;
            case 3:
                mealTV = (TextView) view.findViewById(R.id.d1);
                break;
            default:
                mealTV = (TextView) view.findViewById(R.id.l1);
                break;
        }

        int index = ((ViewGroup) mealTV.getParent()).indexOfChild(mealTV);
        Log.d("Sam", "So i made it to the index: " + index);

        String[] data = mealType.split("::");
        int dataSize = data.length;
        Log.d("Sam", "DataSize " + dataSize);
        if (dataSize < 5) {
            Log.d("Sam", "Data[0] " + data[0]);
            return;
        }


        for (int i = 0; i < dataSize;) {
            TableRow tableRow = new TableRow(mainActivity);

            TextView itemNameTV = new TextView(mainActivity);
            setLayoutParams(itemNameTV);

            TextView caloriesTV =  new TextView(mainActivity);
            setLayoutParams(caloriesTV);

            TextView totalFatTV =  new TextView(mainActivity);
            setLayoutParams(totalFatTV);

            TextView carbsTV = new TextView(mainActivity);
            setLayoutParams(carbsTV);

            TextView proteinTV =  new TextView(mainActivity);
            setLayoutParams(proteinTV);

            Log.d("Sam", "So i made it to the text: " + data[i]);
            itemNameTV.setText(data[i++]);
            Log.d("Sam", "So i made it to the text: " + data[i]);
            caloriesTV.setText(data[i++]);
            Log.d("Sam", "So i made it to the text: " + data[i]);
            totalFatTV.setText(data[i++]);
            Log.d("Sam", "So i made it to the text: " + data[i]);
            carbsTV.setText(data[i++]);
            Log.d("Sam", "So i made it to the text: " + data[i]);
            proteinTV.setText(data[i++]);

            tableRow.addView(itemNameTV, 0);
            tableRow.addView(caloriesTV, 1);
            tableRow.addView(totalFatTV, 2);
            tableRow.addView(carbsTV, 3);
            tableRow.addView(proteinTV, 4);


            tableLayout.addView(tableRow, index+1);


        }

    }

    public void setLayoutParams(TextView view) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(0,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
//        params.gravity = 17;
        view.setPadding(5, 20, 0, 20);
        view.setLayoutParams(params);
        view.setGravity(17); //center
        view.setTextColor(getResources().getColor(R.color.primaryColorDark));
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void setHealthPreference(String key, String value) {
        SharedPreferences preferences = getActivity().getSharedPreferences(HEALTH_PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);

//        valueString = preferences.getString(keyString, "");
        editor.apply();
    }

    private String getHealthPreference(String key, String defaultValue) {
        SharedPreferences preferences = getActivity().getSharedPreferences(HEALTH_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(key, defaultValue);
    }
//
//    private void getBackendPrefs() {
//        SharedPreferences preferences = getActivity().getSharedPreferences(BACKEND_PREF_FILE_NAME, Context.MODE_PRIVATE);
//        day = preferences.getInt("day", 0) - 1;
//    }

}