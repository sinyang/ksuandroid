package com.infinity.KSUAndroid;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.ListView;
import android.widget.Toast;
import com.infinity.KSUAndroid.ArrayAdapters.MenuArrayAdapter;
import com.infinity.KSUAndroid.ArrayAdapters.VenueArrayAdapter;
import com.infinity.KSUAndroid.GeneralClasses.CommonsAPI;
import com.infinity.KSUAndroid.GeneralClasses.FoodItem;
import com.infinity.KSUAndroid.GeneralClasses.NoNetworkNotification;
import com.infinity.KSUAndroid.GeneralClasses.Venue;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class GenericListViewFragment extends ListFragment implements AdapterView.OnItemClickListener
{
	MainActivity mainActivity;
	Future<ArrayList<?>> future;
	ArrayList<?> returnedArrayList;
	int[] images;
	ListView itemList;
	CommonsAPI c;
	TextView venueTitle;
	String PREF_FILE_NAME = "backendPrefs";
	private CommonFunctions listener;


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if(activity instanceof CommonFunctions) {
			listener = (CommonFunctions)activity;
		}
//		listener.startProgressSpinner();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mainActivity = (MainActivity) getActivity();
//		listener.startProgressSpinner();
		Log.d("JIM","GenericListViewFragment Oncreate");

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d("JIM","GenericListViewFragment START");
		View view = inflater.inflate(R.layout.generic_listview_fragment, container, false);

		/*make sure the day/meal chooser drawer button is visible*/
		Toolbar toolbar = ((MainActivity) getActivity()).getToolbar();
		TextView actionBarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title_text);
		View dayMealButton = toolbar.findViewById(R.id.toolbar_right_drawer_button);
		actionBarTitle.setText(R.string.menuCommons);
		dayMealButton.setVisibility(View.VISIBLE);


		Log.d("SAM","Spinner START");
		itemList = (ListView) view.findViewById(android.R.id.list);
		venueTitle = (TextView) view.findViewById(R.id.venue_name);
		venueTitle.setText(mainActivity.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).getString("venueFullName",""));
		mainActivity.actionBarTitle.setText(R.string.menuCommons);

		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		//call CommonsAPI ASAP, give the network thread as much time as possible
		ExecutorService service =  Executors.newSingleThreadExecutor();
		Log.d("JIM","GenericListViewFragment 1");
		c = new CommonsAPI(mainActivity, mainActivity.findViewById(R.id.generic_list_view_fragment_layout));
		Log.d("JIM","GenericListViewFragment 2");
		future = service.submit(c);

		super.onActivityCreated(savedInstanceState);
		
		try
		{
			returnedArrayList = future.get();
		}
		catch (Exception e)
		{
			Log.d("JIM","in CATCH" + e);
			e.printStackTrace();
			mainActivity.getSupportFragmentManager().popBackStack();
			new NoNetworkNotification().show(mainActivity.getFragmentManager(), "");
		}
		
		images = setImageArray();
		
		Log.d("JIM","GenericListViewFragment onActivityCreated prolly fail ear");
		if (c.getWhichFragment() == 0) {
			VenueArrayAdapter adapter = new VenueArrayAdapter(mainActivity,arrayListtoArray(returnedArrayList),images);
			itemList.setAdapter(adapter);
		} else {
			itemList.setDividerHeight((int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
			MenuArrayAdapter adapter = new MenuArrayAdapter(mainActivity,arrayListtoArray(returnedArrayList));
			itemList.setAdapter(adapter);
		}

		Log.d("SAM","Spinner Gone");
//		spinner.setVisibility(View.GONE);
		Log.d("JIM","GenericListViewFragment DONE");
		getListView().setOnItemClickListener(this);
//		listener.stopProgressSpinner();
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapterview, View view, int i, long l)
	{
		Log.d("JIM","GenericListViewFragment onItemClick BEGIN");
		Fragment frag = null;
		Log.d("JIM","GenericListViewFragment onItemClick 1");
		Object obj = returnedArrayList.get(i);
		Log.d("JIM","GenericListViewFragment onItemClick 2");
		if(obj instanceof Venue)
		{
			Venue v = (Venue) obj;
			saveBackendPrefs("venue",getVenue(v.getName()));
			saveBackendPrefs("venueFullName", v.getName());
			Log.d("JIM","GenericListViewFragment onItemClick 2");
			frag = new GenericListViewFragment();
		}
		else if(obj instanceof FoodItem)
		{
			FoodItem fi = (FoodItem) obj;
			saveBackendPrefs("pkey", fi.getpkey());
			frag = new NutritionFragment();

		}
		else
		{
			Toast.makeText(mainActivity, "What the "+returnedArrayList.get(i)+" just happened", Toast.LENGTH_LONG).show();
		}
		Log.d("JIM","GenericListViewFragment onItemClick 3");
		if (c.getWhichFragment() == 1) {
			listener.changeFragment(frag);

		} else {
			listener.changeFragment(frag);
		}
		Log.d("JIM","GenericListViewFragment onItemClick END");
	}
	
	// Save preferences
	private void saveBackendPrefs(String key, int value)
	{
		SharedPreferences preferences = mainActivity.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putInt(key, value);
		editor.commit();
	}
	private void saveBackendPrefs(String key, String value)
  	{
  		SharedPreferences preferences = mainActivity.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
  		Editor editor = preferences.edit();
  		editor.putString(key, value);
  		editor.commit();
  	}
  	private String[] arrayListtoArray(ArrayList<?> a)
  	{
  		if(a == null)
  		{
  			return new String[0];
  		}
  		String s[] = new String[a.size()];
  		for(int i =0;i<a.size();i++)
  		{
  			s[i]=a.get(i).toString();
  		}
  		return s;
  	}
  	private String getVenue(String v)
	{
		return v.substring(0, 3).toLowerCase(Locale.ENGLISH);
	}
  	private int[] setImageArray()
  	{
  		if(returnedArrayList==null)
  		{
  			return new int[0];
  		}
  		ArrayList<Integer> im = new ArrayList<Integer>();
  		Log.d("JIM","returned arraylist size  :"+returnedArrayList.size());
  		for(Object o : returnedArrayList)
  		{
  			if(o.toString().equals("Hwy41"))
  			{
  				im.add(R.drawable.hwy41);
  			}
  			else if(o.toString().equals("Piati"))
  			{
  				im.add(R.drawable.piati);
  			}
  			else if(o.toString().equals("Apron Strings") || o.toString().equals("Entre"))
  			{
  				im.add(R.drawable.apronstrings);
  			}
  			else if(o.toString().equals("Campus Green"))
  			{
  				im.add(R.drawable.campusgreen);
  			}
  			else if(o.toString().equals("Wok Your Way"))
  			{
  				im.add(R.drawable.wokyourway);
  			}
  			else if(o.toString().equals("Globetrotter"))
  			{
  				im.add(R.drawable.globetrotter);
  			}
  			else if(o.toString().equals("Dan's Deli") || o.toString().equals("Pizza"))
  			{
  				im.add(R.drawable.dansdeli);
  			}
  			else if(o.toString().equals("Breakfast"))
  			{
  				im.add(R.drawable.breakfast);
  			}
  			else if(o.toString().equals("Soup"))
  			{
  				im.add(R.drawable.soup);
  			}
  			else if(o.toString().equals("Meat"))
  			{
  				im.add(R.drawable.meat);
  			}
  			else if(o.toString().equals("Bread") || o.toString().equals("Grill"))
  			{
  				im.add(R.drawable.bread);
  			}

  			else if(o.toString().equals("Stone Mill Bakery"))
  			{
  				im.add(R.drawable.stonemillbakery);
  			}
  			else if(o.toString().equals("Vegetarian/Vegan"))
  			{
  				im.add(R.drawable.campusgreen);
  			}
  			else
  			{
  				im.add(R.drawable.soup);
  			}
  		}
  		int[] returnme = new int[im.size()];
  		Log.d("JIM","images size :"+im.size());
  		for(int i =0 ; i<im.size();i++)
  		{
  			returnme[i]=im.get(i);
  		}
  		return returnme;
  	}
}

