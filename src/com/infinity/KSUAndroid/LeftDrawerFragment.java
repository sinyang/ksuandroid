package com.infinity.KSUAndroid;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;
import com.infinity.KSUAndroid.ArrayAdapters.DrawerArrayAdapter;


/**
 * Created by Sam.I on 12/16/2014.
 */
public class LeftDrawerFragment extends Fragment {
    static MainActivity mainActivity;
    private String[] drawerOptionsStrings;
    ListView drawerListView;
    private DrawerLayout mDrawerLayout;

    /*
     * mUserLearnedDrawer is used to determined if the user has seen
     * the drawer. We want the drawer to show in the app the
     * very first time its ran so that the user can know its there.
     * However, once the user has seen the drawer, we dont want
     * it to open on app startup.
     * This will be saved in the sharedPreferences of the device.
     */

    private boolean mUserLearnedDrawer;
    public static final String PREF_FILE_NAME = "backendPrefs";
    public static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";
    private boolean mFromSavedInstanceState;

    private View containerView;

    private CommonFunctions commonFunctions;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof CommonFunctions) {
            commonFunctions = (CommonFunctions)activity;
        }
    }


    public LeftDrawerFragment() {
        //required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mainActivity = (MainActivity) getActivity();
        drawerOptionsStrings = getResources().getStringArray(R.array.drawer_listview_array);

        //default value set to false, meaning user has never seen the drawer
        mUserLearnedDrawer = Boolean.valueOf(readFromPreferences(mainActivity, KEY_USER_LEARNED_DRAWER, "false"));

        if(savedInstanceState != null) {
            //if its not null that means activity was opened, but screen was probably rotated, etc...
            mFromSavedInstanceState = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        drawerListView = (ListView) view.findViewById(R.id.left_drawer_list);
        drawerListView.setAdapter(new DrawerArrayAdapter(mainActivity,drawerOptionsStrings));


        drawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                /*
                } else if (i == 1) {
                    openMealTracker();

                } else if (i == 2) {
                    openAllergens();
                } else if (i == 3) {
                    //allergens

                } else if (i == 4) {
                    openFeedbackView();
                    mainActivity.actionBarTitle.setText(R.string.app_name);
                }*/
                if (i == 0) {
                    saveBackendPrefs("pkey", 0);
                    saveBackendPrefs("venue", "");
                    saveBackendPrefs("venueFullName", "");
                    GenericListViewFragment genericListViewFragment = new GenericListViewFragment();

                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    for(int j = 0; j < fm.getBackStackEntryCount(); j++) {
                        fm.popBackStack();
                    }
                    commonFunctions.changeFragment(genericListViewFragment);
                    mainActivity.actionBarTitle.setText(R.string.menuCommons);
                    closeDrawer();
                } else if (i == 1) {
                    openMealTracker();

                } else if (i == 2) {
                    openAllergens();
                }else if (i == 3) {
                    openFeedbackView();
                    mainActivity.actionBarTitle.setText(R.string.app_name);
                }

            }
        });

        return view;
    }


    public void setUp(int fragmentID, DrawerLayout drawerLayout, Toolbar toolbar) {
        containerView = mainActivity.findViewById(fragmentID);
        mDrawerLayout = drawerLayout; //sets as the layout that was passed



        if(!mUserLearnedDrawer &&!mFromSavedInstanceState) {
            mainActivity.drawerStatus = 1;
            openDrawer();
        }

    }

    void openDrawer() {
        mDrawerLayout.openDrawer(containerView);
        if(!mUserLearnedDrawer){
            mUserLearnedDrawer = true;
            saveBackendPrefs(KEY_USER_LEARNED_DRAWER, "" + mUserLearnedDrawer);
        }
    }

    void closeDrawer() {
        mDrawerLayout.closeDrawer(containerView);
    }

    private void openMealTracker() {
        MealTrackerFragment mealTrackerFragment = new MealTrackerFragment();
        commonFunctions.pushFragment(mealTrackerFragment);
        closeDrawer();


    }

    private void openFeedbackView() {
        FeedbackFragment feedbackFragment = new FeedbackFragment();
        commonFunctions.pushFragment(feedbackFragment);
        closeDrawer();
    }

    private void openAllergens()
    {
        final String[] prefArray = getResources().getStringArray(R.array.allergenarray);
        boolean[] checkedItems = new boolean[prefArray.length];

        for (int i = 0; i < prefArray.length; i++)
        {
            prefArray[i] = Character.toUpperCase(prefArray[i].charAt(0))+ prefArray[i].substring(1);
            checkedItems[i] = loadAllergens(prefArray[i]);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);

        builder.setTitle("Select allergens to avoid");
        builder.setMultiChoiceItems(prefArray, checkedItems,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            saveAllergens(prefArray[which], true);
                        } else {
                            // Else, if the item is already in the array, remove it
                            saveAllergens(prefArray[which], false);
                        }
                    }

                })
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(mainActivity.getApplicationContext(), "Preferences saved", Toast.LENGTH_LONG).show();
                        if (mainActivity.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).getInt("pkey", 0) > 0) {
                            saveBackendPrefs("pkey", 0);
                            saveBackendPrefs("venue", "");
                            saveBackendPrefs("venueFullName", "");
                        }
                        commonFunctions.changeFragment(new GenericListViewFragment());
                    }
                }).setNegativeButton("Cancel", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    // Save preferences
    private static void saveAllergens(String key, boolean value)
    {
        SharedPreferences preferences = mainActivity.getSharedPreferences("allergens", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    // get saved preferences
    private static boolean loadAllergens(String key)
    {
        SharedPreferences preferences = mainActivity.getSharedPreferences("allergens", Context.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }

    //reading from sharedPreference
    //takes similar parameters but you take in a default value incase a previous value did not exist.
    public static String readFromPreferences(Context context, String preferenceName, String defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);

    }

    private static void saveBackendPrefs(String key, int value)
    {
        SharedPreferences preferences = mainActivity.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }
    private static void saveBackendPrefs(String key, String value)
    {
        SharedPreferences preferences = mainActivity.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

}