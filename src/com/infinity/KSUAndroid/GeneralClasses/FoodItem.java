package com.infinity.KSUAndroid.GeneralClasses;

import java.util.ArrayList;

/*
 * Michael Hug
 * michael@webreadllc.com
 * 28 June 2014
 * 
 * This class represents the FoodItems that are used in MenuFragment.
 * 
 */

public class FoodItem 
{
	private String name;
	private int pkey;
	private ArrayList<String> vegbal;
	
	public FoodItem(String pkey, String name, String veg_type, String bal_type)
	{
		this.name = name;
		this.pkey = Integer.parseInt(pkey);
		
		this.vegbal = new ArrayList<String>();
		this.vegbal.add(veg_type);
		this.vegbal.add(bal_type);
	}
	
	public String getFoodItemName()
	{
		return this.name;
	}
	
	public int getpkey()
	{
		return this.pkey;
	}
	
	public ArrayList<String> getFoodItemVegBalInfo()
	{
		return this.vegbal;
	}
	
	//ListViews call the class's toString()
	public String toString()
	{ 
	    return name;
	}
}