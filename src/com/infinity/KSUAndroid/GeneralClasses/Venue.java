package com.infinity.KSUAndroid.GeneralClasses;

/*
 * Michael Hug
 * michael@webreadllc.com
 * 28 June 2014
 * 
 * This class represents the Venues that are used in VenueFragment
 * 
 */

public class Venue
{	
	private String name;
	
	public Venue(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	//ListViews call the class's toString()
	public String toString()
	{ 
	    return name;
	}
}