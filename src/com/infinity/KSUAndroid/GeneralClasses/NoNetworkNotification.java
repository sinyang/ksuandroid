package com.infinity.KSUAndroid.GeneralClasses;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;
import com.infinity.KSUAndroid.R;

public class NoNetworkNotification extends DialogFragment
{

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
    	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    	builder.setTitle(getResources().getString(R.string.networkerror));
    	builder.setMessage(getResources().getString(R.string.checkconnection));
    	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() 
    	{
    		public void onClick(DialogInterface dialog, int which)
            {
                System.exit(1);
            }
    	}
        );
    	AlertDialog dialog = builder.show();
    	TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
    	messageText.setGravity(Gravity.CENTER);
    	dialog.show();
        return dialog;
    }
}
