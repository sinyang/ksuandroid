package com.infinity.KSUAndroid.GeneralClasses;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

/**
 * Created by Sam.I on 1/29/2015.
 */
public class Installation {
    private static String uID = null;
    private static final String INSTALLATION = "INSTALLATION";

    public synchronized static String id (Context context) {
        if (uID == null) {
            File installation = new File(context.getFilesDir(), INSTALLATION);
            try {
                if(!installation.exists()){
                    writeInstallationFile(installation);
                }
                uID = readInstallationFile(installation);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        saveIDToPref(uID, context);

        return uID;
    }

    private static void saveIDToPref (String id, Context context) {
        SharedPreferences preferences = context.getSharedPreferences("backendPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("uID", id);
        editor.apply();
    }

    private static String readInstallationFile (File installation) throws IOException {
        RandomAccessFile file = new RandomAccessFile(installation, "r");
        byte[] bytes = new byte[(int)file.length()];
        file.readFully(bytes);
        file.close();
        return new String(bytes);
    }

    private static void writeInstallationFile (File installation) throws IOException {
        FileOutputStream out = new FileOutputStream(installation);
        String id = UUID.randomUUID().toString();
        out.write(id.getBytes());
        out.close();
    }

}
