package com.infinity.KSUAndroid.GeneralClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.infinity.KSUAndroid.R;
import org.jsoup.Jsoup;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

/*
 * Michael Hug
 * michael@webreadllc.com
 * 28 June 2014
 * 
 * This class will access the server i setup.
 * 
 */
public class CommonsAPI implements Callable<ArrayList<?>>
{
	private String baseURL;
	private int pkey;
	private int day;
	private int meal;
	private String venue;
	private String venueFullName;
	private Context context;
	private int whichFrag = 0;
	View fragLayout;


	//constructor
	public CommonsAPI(Context context, View fragLayout)
	{
		this.context=context;
		this.fragLayout = fragLayout;
		Log.d("JIM","CommonsAPI begin");
		SharedPreferences preferences = context.getSharedPreferences("backendPrefs", Context.MODE_PRIVATE);
		Log.d("JIM","CommonsAPI 1");
		pkey = preferences.getInt("pkey", 0);
		Log.d("JIM","CommonsAPI 2 pkey: " + pkey);
		day = preferences.getInt("day", 0);
		Log.d("JIM","CommonsAPI 3");
		meal = preferences.getInt("meal", 0);
		Log.d("JIM","CommonsAPI 4");
		venue = preferences.getString("venue", "");
		Log.d("JIM","CommonsAPI 5");
//		baseURL = "http://webreadllc.com/query.php?u="+Secure.getString(context.getContentResolver(),Secure.ANDROID_ID)+"&";
		String uID = preferences.getString("uID", "");
		baseURL = "http://ideafragments.com/menu_app/webread/query/query.php?u=" + (uID.equals("") ? new Installation().id(context) : uID) +"&";
		Log.d("JIM","CommonsAPI end");
		venueFullName = preferences.getString("venueFullName", "");
	}

	//constructor
	public CommonsAPI(Context context)
	{
		this.context=context;
		Log.d("JIM","CommonsAPI begin");
		SharedPreferences preferences = context.getSharedPreferences("backendPrefs", Context.MODE_PRIVATE);
		Log.d("JIM","CommonsAPI 1");
		pkey = preferences.getInt("pkey", 0);
		Log.d("JIM","CommonsAPI 2 pkey: " + pkey);
		day = preferences.getInt("day", 0);
		Log.d("JIM","CommonsAPI 3");
		meal = preferences.getInt("meal", 0);
		Log.d("JIM","CommonsAPI 4");
		venue = preferences.getString("venue", "");
		Log.d("JIM","CommonsAPI 5");
		baseURL = "http://ideafragments.com/menu_app/webread/query/query.php?u="+Secure.getString(context.getContentResolver(),Secure.ANDROID_ID)+"&";
		Log.d("JIM","CommonsAPI end");
		venueFullName = preferences.getString("venueFullName", "");
	}
	
	@Override
	//pass exceptions up. That way the UI thread can handle it... toast it, fail, exit.... do what you want
	public ArrayList<?> call() throws IOException
	{
		Log.d("JIM","Callable begin frag: " + whichFrag);
		
		ArrayList<?> rtn = null;
		
		//this is some solid ass logic right ear
		if(pkey>0)
		{
			whichFrag = 2;
			Log.d("JIM","Callable nut");
			rtn =  getNutrition();
		}
		else if(day>0 && meal>0)
		{
			ImageView imV = (ImageView) fragLayout.findViewById(R.id.menu_view_venue_image);
			if(venue.equals(""))
			{
				Log.d("JIM","Callable venue"+ "'"+venue+"'");

				//we are in venue list so the extra image and view for food item list should be hidden
				imV.setVisibility(View.GONE);
				fragLayout.findViewById(R.id.menu_view_venue_text_bg).setVisibility(View.GONE);
				rtn = getOpenVenues();

			}
			else
			{
				whichFrag = 1;
				imV.setBackgroundResource(getImageSRC(venueFullName));
				Log.d("JIM","Callable Menu"+ "'"+venue+"'");
				rtn = getMenu();
			}
		}
		Log.d("JIM","Callable END frag: " + whichFrag);
		return rtn;
	}

	public int getWhichFragment() {

		return whichFrag;
	}
	
	//return a list of venues
	private ArrayList<Venue> getOpenVenues() throws IOException
	{
		/*
		 * the following lines of code are used to retrieve the open
		 * venues for the day, based on the users allergen settings
		 * which affect the url of the string sent to the server for
		 * venue retrieval.
		 */
		ArrayList<Venue> rtn = new ArrayList<Venue>();
		String allergens = getAllergenString();

		String url;
		if(allergens.equals(""))
		{
			url =baseURL+"d="+day+"&m="+meal;
		}
		else
		{
			url =baseURL+"d="+day+"&m="+meal+"&a="+allergens;
		}

		List<String> strList =pars(url); //add data from server into list

		//for each string, make a venue object out of it and add
		//to return arraylist
		for (String s : strList)
		{
			rtn.add(new Venue(s));
		}
		return rtn;
	}
	
	//return a list of menu
	private ArrayList<FoodItem> getMenu() throws IOException
	{
		String url;
		ArrayList<FoodItem> rtn = new ArrayList<FoodItem>();
		String allergens = getAllergenString();

		if(allergens.equals(""))
		{
			url=baseURL+"d="+day+"&m="+meal+"&v="+getVenue(venue);
		}
		else
		{
			url=baseURL+"d="+day+"&m="+meal+"&a="+allergens+"&v="+getVenue(venue);
		}

		List<String> strList =pars(url);

		for(int i =0;i<strList.size();)
		{
			rtn.add(new FoodItem(strList.get(i++),strList.get(i++),strList.get(i++),strList.get(i++)));
		}
		return rtn;
	}
	
	///needs logic added
	private ArrayList<String> getNutrition() throws IOException
	{
		return pars(baseURL+"k="+pkey);
	}
	

	//I tired to simplify this and use string.split.... fails...
	private ArrayList<String> pars(String url) throws IOException
	{
		Log.d("JIM","URL = '"+url+"'");
		String js = Jsoup.connect(url).get().html();
		String DownloadedPlaintext = Jsoup.parse(js).text();

		String buf = "";
		ArrayList<String> strList = new ArrayList<String>();
		for(int i=0;i<DownloadedPlaintext.length();i++)
		{
			if(DownloadedPlaintext.charAt(i) == '|')
			{
				strList.add(buf);
				buf = "";
			}
			else
			{
				buf+=DownloadedPlaintext.charAt(i);
			}
		}
		return strList;
	}
	
	private String getAllergenString()
	{
		String returnme = "";
		String[] apiAllergenString = {"egg","fis","glu", "mil", "MSG", "mus", "pea", "she", "soy", "tre", "whe"};
		String[] allergenArray = context.getResources().getStringArray(R.array.allergenarray);
		for(int i =0;i<allergenArray.length;i++)
		{
			if(loadSavedPreferences(allergenArray[i]))
			{
				returnme+=apiAllergenString[i];
			}
		}
		return returnme;
	}
	// get saved preferences
	private boolean loadSavedPreferences(String key)
	{
		SharedPreferences preferences = context.getSharedPreferences("allergens", Context.MODE_PRIVATE);
		return preferences.getBoolean(key, false);
	}
	//the API only needs the first three chars
	private String getVenue(String v)
	{
		return v.substring(0, 3).toLowerCase(Locale.ENGLISH);
	}

	private int getImageSRC(String venueName)
	{
		if(venueName.equals("Hwy41"))
		{
			return R.drawable.hwy41;
		}
		else if(venueName.equals("Piati"))
		{
			return R.drawable.piati;
		}
		else if(venueName.equals("Apron Strings") || venueName.equals("Entre")) {
			return R.drawable.apronstrings;
		}
		else if(venueName.equals("Campus Green"))
		{
			return R.drawable.campusgreen;
		}
		else if(venueName.equals("Wok Your Way"))
		{
			return R.drawable.wokyourway;
		}
		else if(venueName.equals("Globetrotter"))
		{
			return R.drawable.globetrotter;
		}
		else if(venueName.equals("Dan's Deli") || venueName.equals("Pizza"))
		{
			return R.drawable.dansdeli;
		}
		else if(venueName.equals("Breakfast"))
		{
			return R.drawable.breakfast;
		}
		else if(venueName.equals("Soup"))
		{
			return R.drawable.soup;
		}
		else if(venueName.equals("Meat"))
		{
			return R.drawable.meat;
		}
		else if(venueName.equals("Bread") || venueName.equals("Grill"))
		{
			return R.drawable.bread;
		}

		else if(venueName.equals("Stone Mill Bakery"))
		{
			return R.drawable.stonemillbakery;
		}
		else if(venueName.equals("Vegetarian/Vegan"))
		{
			return R.drawable.campusgreen;
		}
		else
		{
			return R.drawable.soup;
		}
	}
}