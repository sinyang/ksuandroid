package com.infinity.KSUAndroid.ArrayAdapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.infinity.KSUAndroid.R;

/**
 * Created by Sam.I on 12/17/2014.
 */
public class VenueArrayAdapter extends ArrayAdapter<String>
{

    private final Context context;
    private final String[] names;
    private final int[] images;

    public VenueArrayAdapter(Context context, String[] names, int[] images)
    {
        super(context, R.layout.single_row,R.id.venue_item_layout,names);
        this.context = context;
        this.names = names;
        this.images = images;
        Log.d("JIM", "images size :" + images.length);
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent)
    {
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.single_row, parent, false);
        }

        TextView tx = (TextView) rowView.findViewById(R.id.textView1);
        View img = rowView.findViewById(R.id.item_bg_view);
//        parent.setBackgroundResource(images[position]);
        img.setBackgroundResource(images[position]);
        tx.setText(names[position]);

        return rowView;
    }
}