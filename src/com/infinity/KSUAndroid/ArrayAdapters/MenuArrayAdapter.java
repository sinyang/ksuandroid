package com.infinity.KSUAndroid.ArrayAdapters;

/**
 * Created by Sam.I on 12/18/2014.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.infinity.KSUAndroid.R;

/**
 * Created by Sam.I on 12/17/2014.
 */
public class MenuArrayAdapter extends ArrayAdapter<String>
{

    private final Context context;
    private final String[] names;

    public MenuArrayAdapter (Context context, String[] names)
    {
        super(context, R.layout.single_menu_row,R.id.menu_item_layout,names);
        this.context = context;
        this.names = names;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.single_menu_row, parent, false);
        }

        TextView tx = (TextView) convertView.findViewById(R.id.menu_item_textView);
        tx.setText(names[position]);

        return convertView;
    }
}