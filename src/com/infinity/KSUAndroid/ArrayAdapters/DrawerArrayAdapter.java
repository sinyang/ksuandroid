package com.infinity.KSUAndroid.ArrayAdapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.infinity.KSUAndroid.R;

/**
 * Created by Sam.I on 12/16/2014.
 */
public class DrawerArrayAdapter extends ArrayAdapter<String>
{

    private final Context context;
    private final String[] options;
    private char list;
    int day;
    int meal;


    public DrawerArrayAdapter(Context context, String[] options, char list)
    {
        super(context, R.layout.single_drawer_list_item,R.id.list_item_icon,options);
        this.context = context;
        this.options = options;
        this.list = list;
    }
    public DrawerArrayAdapter(Context context, String[] options)
    {
        super(context, R.layout.single_drawer_list_item,R.id.list_item_icon,options);
        this.context = context;
        this.options = options;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent)
    {
        loadBackEndPref();
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.single_drawer_list_item, parent, false);
        }

        //came from daymeal chooser
        if(list == 'd') {
            if(position == day) {
                rowView.setBackgroundResource(R.color.secondaryColorTrans);
            } else {
                rowView.setBackgroundResource(R.color.transparent);
            }
        }
        if(list == 'm') {
            if (position == meal) {
                rowView.setBackgroundResource(R.color.secondaryColorTrans);
            } else {
                rowView.setBackgroundResource(R.color.transparent);
            }
        }

        TextView tx = (TextView) rowView.findViewById(R.id.drawer_list_item_text);
        tx.setText(options[position]);
        return rowView;
    }

    public void loadBackEndPref () {
        final String PREF_FILE_NAME = "backendPrefs";
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        day = (sharedPreferences.getInt("day", 0) - 1);
        meal = (sharedPreferences.getInt("meal", 0) - 1);
    }
}