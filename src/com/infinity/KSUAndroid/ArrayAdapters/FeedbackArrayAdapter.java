package com.infinity.KSUAndroid.ArrayAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.infinity.KSUAndroid.R;

/**
 * Created by Sam.I on 2/8/2015.
 */
public class FeedbackArrayAdapter extends ArrayAdapter<String[]> {

    private final Context context;
    private final String[][] feedbackItemsArray;


    public FeedbackArrayAdapter(Context context, String[][] objects) {
        super(context, R.layout.single_row_feedback, objects);
        this.context = context;
        this.feedbackItemsArray = objects;
        System.out.println(objects.length);
        System.out.println("" + feedbackItemsArray[0].length);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.single_row_feedback, parent, false);
        }

        TextView header = (TextView) convertView.findViewById(R.id.feedback_heading);
        TextView topic = (TextView) convertView.findViewById(R.id.feedback_topic);
        TextView body = (TextView) convertView.findViewById(R.id.feedback_body);

        String[] feedbackItemContents = feedbackItemsArray[position];

        header.setText(feedbackItemContents[0]);
        body.setText(feedbackItemContents[1]);
        topic.setText(feedbackItemContents[2]);

        return convertView;
    }
}
