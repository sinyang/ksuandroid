package com.infinity.KSUAndroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;

/**
 * Created by Sam.I on 12/21/2014.
 */
public interface CommonFunctions {
    public void startProgressSpinner();

    public void stopProgressSpinner();

    public void changeFragment(Fragment frag);

    public void pushFragment(Fragment frag);


}
