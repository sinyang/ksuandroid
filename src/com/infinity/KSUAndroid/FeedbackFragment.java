package com.infinity.KSUAndroid;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.infinity.KSUAndroid.ArrayAdapters.FeedbackArrayAdapter;

import java.util.ArrayList;

/**
 * Created by Sam.I on 2/8/2015.
 */
public class FeedbackFragment extends Fragment {

    private ListView feedbackListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        feedbackListView = (ListView) view.findViewById(R.id.feedback_listview);
        String[][] feedbackOptions = new String[5][];
        feedbackOptions[0] = getResources().getStringArray(R.array.feedback1);
        feedbackOptions[1] = getResources().getStringArray(R.array.feedback2);
        feedbackOptions[2] = getResources().getStringArray(R.array.feedback3);
        feedbackOptions[3] = getResources().getStringArray(R.array.feedback4);
        feedbackOptions[4] = getResources().getStringArray(R.array.feedback5);

        feedbackListView.setAdapter(new FeedbackArrayAdapter(getActivity(), feedbackOptions));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        feedbackListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("hello");
            }
        });
    }
}